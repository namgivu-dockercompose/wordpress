#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)  # SH aka SCRIP_HOME

n=mywordpress_n ; docker network create $n

c=wordpress_c ; docker run -d \
    \
    -e     WORDPRESS_DB_HOST=mariadb_c \
    -e     WORDPRESS_DB_NAME=wordpress \
    -e     WORDPRESS_DB_USER=example \
    -e     WORDPRESS_DB_PASS=examplePW \
    \
    -v "$SH/wordpress-data:/var/www/html" \
    --name $c -h$c \
    --network $n \
    -p 20822:80 wordpress
