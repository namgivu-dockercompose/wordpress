#!/bin/bash
docstring='
similar to
docker-compose down ; docker-compose up -d
'

_SH=$(cd `dirname $BASH_SOURCE` && pwd)  # SH aka SCRIP_HOME

source "$_SH/220822_mymariadb_c/01_stoprm.sh"
source "$_SH/220822_mymariadb_c/00_run.sh"

source "$_SH/220823_wordpress_c/01_stoprm.sh"
source "$_SH/220823_wordpress_c/00_run.sh"
