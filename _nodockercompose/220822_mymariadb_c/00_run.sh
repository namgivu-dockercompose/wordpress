#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)  # SH aka SCRIP_HOME

n=mywordpress_n ; docker network create $n

c=mariadb_c ; docker run -d \
    \
    -e MYSQL_ROOT_PASSWORD=examplerootPW \
    -e       MYSQL_DB_NAME=wordpress \
    -e       MYSQL_DB_USER=example \
    -e       MYSQL_DB_PASS=examplePW \
    \
    -v "$SH/mysql-data:/var/lib/mysql" \
    --name $c -h$c \
    --network $n \
    mariadb
