SH=$(cd `dirname $BASH_SOURCE` && pwd)  # SH aka SCRIP_HOME

docker-compose \
    -p mywordpress \
    --env-file "$SH/dockercompose.env" \
    \
    up -d \
    \
    --remove-orphans
